import {StyleSheet} from "react-native";

export class StyleSheetFactory {
    static getSheet(colors) {
        return StyleSheet.create({
            container: {
                height: '100%',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'center',
            },
            gridContainer: {
                width: 125,
                backgroundColor: "red",
                display: "flex",
                flexDirection: "row",
                flexWrap: "wrap"
            },
            menuButton: {
                width: 200,
                height: 60,
                margin: 5,
                borderRadius: 5,
                backgroundColor: colors.primary,
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
            },
            buttonText: {
                color: colors.text,
                fontSize: 18,
                fontWeight: "500"
            },
            AuthScreen: {
                height: '100%',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'center',
            },
            AuthMainButtonContainer: {
                marginTop: 20
            },
            InputField: {
                color: colors.text,
                width: 200,
                height: 45,
                borderRadius: 5,
                padding: 15,
                backgroundColor: colors.secondary
            },
            InputFieldTitle: {
                color: colors.text,
                marginLeft: 5,
                marginTop: 10
            },
            InputFieldContainer: {
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'center',
            },
            AuthTitle: {
                color: colors.text,
                fontSize: 25,
                fontWeight: "600",
                marginBottom: 5
            },
            ErrorMessage: {
                color: colors.error,
                marginLeft: 5,
            },

            gameContainer: {
                flex: 1,
                alignSelf: 'stretch',
                backgroundColor: colors.gameBackground
            },
            gameRow: {
                flex: 1,
                flexDirection: 'row'
            },
            gameBody: {
                flex: 18,
                justifyContent: 'space-between',
                paddingLeft: 20,
                paddingRight: 20,
                paddingBottom: 20,
                paddingTop: 10,
            },

            timeAndScoreContainer: {
                display: 'flex',
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center'
            },
            timeScore: {
                fontSize: 35,
                padding: 10,
                marginRight: 20,
                color: colors.text,
                alignItems: 'center',
                justifyContent: 'center'
            },
            score: {
                fontSize: 35,
                padding: 10,
                color: colors.text,
            },

            score_container: {
                flex: 1,
                alignItems: 'center',
                padding: 10
            },
            // score: {
            //     color: colors.text,
            //     fontSize: 40,
            //     fontWeight: 'bold'
            // },
            gameHeader: {
                flex: 1,
                flexDirection: 'column',
                alignSelf: 'stretch',
                paddingTop: 20,
                paddingBottom: 5,
                backgroundColor: colors.gameBackground
            },
            gameHeaderText: {
                fontWeight: 'bold',
                fontSize: 17,
                textAlign: 'center',
                color: colors.text
            },

            statisticsContainer: {
                flex: 1,
            },
            statisticsItem: {
                backgroundColor: colors.primary,
                padding: 20,
                marginVertical: 8,
                marginHorizontal: 16
            },

            statisticsItemTitle: {
                fontSize: 32,
                color: colors.text
            },

            card: {
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
                borderWidth: 1,
                borderRadius: 10,
                backgroundColor: colors.cardBackground,
                margin: 5,
                borderColor: colors.text,
            },
            card_text: {
                fontSize: 50,
                fontWeight: 'bold'
            }
        })
    }
}

const API_KEY="AIzaSyCVbD-P1wT42sErkIpYYoz9_b3bPNfxnx8";
const AUTH_DOMAIN="mobile-lab-1.firebaseapp.com";
const DATABASE_URL="https://mobile-lab-1.firebaseio.com";
const PROJECT_ID="mobile-lab-1";
const STORAGE_BUCKET="mobile-lab-1.appspot.com";
const MESSAGING_SENDER_ID="837900146916";
const APP_ID="1:837900146916:web:0ee9980c94b8ce22cf34f1";
const MEASUREMENT_ID="G-D7BGEMX9QJ";

const environments = {
    API_KEY,
    AUTH_DOMAIN,
    DATABASE_URL,
    PROJECT_ID,
    STORAGE_BUCKET,
    MESSAGING_SENDER_ID,
    APP_ID,
    MEASUREMENT_ID,
};

export default environments;

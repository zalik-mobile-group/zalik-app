import * as firebase from 'firebase';
import environments from '../../enviroments'
import 'firebase/firestore';

const firebaseConfig = {
    apiKey: environments.API_KEY,
    authDomain: environments.AUTH_DOMAIN,
    databaseURL: environments.DATABASE_URL,
    projectId: environments.PROJECT_ID,
    storageBucket: environments.STORAGE_BUCKET,
    messagingSenderId: environments.MESSAGING_SENDER_ID,
    appId: environments.APP_ID,
    measurementId: environments.MEASUREMENT_ID
};

firebase.initializeApp(firebaseConfig);

export default firebase;

import React, {useEffect} from 'react';
import * as Yup from 'yup';
import {Formik} from 'formik';
import {Alert, View, TextInput, Text, TouchableOpacity} from 'react-native'
import firebase from "../../../config/firebase/firebase";
import {StyleSheetFactory} from "../../../styles/styles";
import {useTheme} from "@react-navigation/native";
import Button from "../../../components/Button"
import InputField from "../../../components/InputField";


const Login = ({navigation}) => {
    const {colors} = useTheme();
    const styles = StyleSheetFactory.getSheet(colors);

    const schema = Yup.object({
        email: Yup.string().email('Email is invalid').required('Email is required'),
        password: Yup
            .string()
            .required('Password is required')
            .matches(
                /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/,
                `8 Characters\nOne Uppercase\nOne Lowercase\nOne Number\nOne special case Character`)

    });

    const submit = (values) => {
        firebase
            .auth()
            .signInWithEmailAndPassword(values.email, values.password)
            .then((res) => {
                console.log(res);
                console.log('User logged-in successfully!');
                navigation.reset({
                    index: 0,
                    routes: [{name: "Main"}],
                });
            })
            .catch(error => {
                Alert.alert(
                    "Error",
                    `${error}`,
                    [
                        {
                            text: "OK", onPress: () => {
                            }
                        }
                    ],
                    {cancelable: true}
                );
            })
    };

    useEffect(() => {
        firebase.auth().onAuthStateChanged((user) => {
            if (user) {
                navigation.reset({
                    index: 0,
                    routes: [{name: "Main"}],
                });
            }
        })
    }, []);

    return (
        <View style={styles.AuthScreen}>
            <Text style={styles.AuthTitle}>Login</Text>
            <Formik
                initialValues={{email: '', password: ''}}
                validationSchema={schema}
                onSubmit={(values) => {
                    submit(values)
                }}
            >
                {({errors, touched, handleChange, handleBlur, handleSubmit, values}) => (
                    <View>
                        <InputField
                            title="Email"
                            change={handleChange('email')}
                            blur={handleBlur('email')}
                            placeholder="Enter email"
                            value={values.email}
                            touched={touched.email}
                            error={errors.email}
                        />
                        <InputField
                            title="Password"
                            change={handleChange('password')}
                            blur={handleBlur('password')}
                            placeholder="Enter password"
                            value={values.password}
                            touched={touched.password}
                            error={errors.password}
                        />
                        <View style={styles.AuthMainButtonContainer}>
                            <Button
                                btnStyles={styles.menuButton}
                                onClick={(values) => handleSubmit(values)}
                                btnTextStyles={styles.buttonText}
                                title="Login"
                            />
                        </View>
                    </View>
                )}
            </Formik>
            <View>
                <Button
                    btnStyles={styles.menuButton}
                    onClick={() => navigation.navigate('Sign Up')}
                    btnTextStyles={styles.buttonText}
                    title="Go to Signup"
                />
            </View>
        </View>
    )
};

export default Login

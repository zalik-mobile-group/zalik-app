import React from 'react';
import * as Yup from 'yup';
import {Formik} from 'formik';
import {Alert, View, TextInput, Text, TouchableOpacity} from 'react-native'
import firebase from "../../../config/firebase/firebase";
import {StyleSheetFactory} from "../../../styles/styles";
import {useTheme} from "@react-navigation/native";
import Button from "../../../components/Button"
import InputField from "../../../components/InputField";


const SignUp = ({navigation}) => {
    const {colors} = useTheme();
    const styles = StyleSheetFactory.getSheet(colors);

    const schema = Yup.object({
        name: Yup
            .string()
            .min(1, 'Name needs to be at least 1 char')
            .max(20, 'Name cannot exceed 20 char')
            .required('Name is required'),
        email: Yup.string().email('Email is invalid').required('Email is required'),
        password: Yup
            .string()
            .required('Password is required')
            .matches(
                /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/,
                `8 Characters\nOne Uppercase\nOne Lowercase\nOne Number\nOne special case Character`)
    });

    const onSignUp = (values) => {
        firebase
            .auth()
            .createUserWithEmailAndPassword(values.email, values.password)
            .then((res) => {
                res.user.updateProfile({
                    displayName: values.name,
                }).then(() => {
                        console.log('User registered successfully!');
                        firebase
                            .auth()
                            .signInWithEmailAndPassword(values.email, values.password)
                            .then((res) => {
                                console.log(res);
                                // props.navigation.navigate('App')
                            })
                            .catch(error => {
                                Alert.alert(
                                    "Error",
                                    `${error}`,
                                    [
                                        {text: "OK", onPress: () => {}}
                                    ],
                                    {cancelable: true}
                                );
                            })
                    }
                );
            })
            .catch(error =>
                Alert.alert(
                    "Error",
                    `${error}`,
                    [
                        {text: "OK", onPress: () => {}}
                    ],
                    {cancelable: true}
                )
            )
    };

    return (
        <View style={styles.AuthScreen}>
            <Text style={styles.AuthTitle}>Sign Up</Text>
            <Formik
                initialValues={{name: '', email: '', password: ''}}
                validationSchema={schema}
                onSubmit={(values) => {
                    onSignUp(values)
                }}
            >
                {({errors, touched, handleChange, handleBlur, handleSubmit, values}) => (
                    <View>
                        <InputField
                            title="Name"
                            change={handleChange('name')}
                            blur={handleBlur('name')}
                            placeholder="Enter name"
                            value={values.name}
                            touched={touched.name}
                            error={errors.name}
                        />
                        <InputField
                            title="Email"
                            change={handleChange('email')}
                            blur={handleBlur('email')}
                            placeholder="Enter email"
                            value={values.email}
                            touched={touched.email}
                            error={errors.email}
                        />
                        <InputField
                            title="Password"
                            change={handleChange('password')}
                            blur={handleBlur('password')}
                            placeholder="Enter password"
                            value={values.password}
                            touched={touched.password}
                            error={errors.password}
                        />
                        <View style={styles.AuthMainButtonContainer}>
                            <Button
                                btnStyles={styles.menuButton}
                                onClick={(values) => handleSubmit(values)}
                                btnTextStyles={styles.buttonText}
                                title="Sign Up"
                            />
                        </View>
                    </View>
                )}
            </Formik>
            <View>
                <Button
                    btnStyles={styles.menuButton}
                    onClick={() => navigation.navigate('Sign In')}
                    btnTextStyles={styles.buttonText}
                    title="Go to Login"
                />
            </View>
        </View>
    )
};

export default SignUp;

import React from 'react';
import {createStackNavigator} from "@react-navigation/stack";
import Login from "./components/Login";
import SignUp from "./components/SignUp";

export function AuthNavigator() {
    return (
        <Stack.Navigator screenOptions={{ headerShown: false }}>
            <Stack.Screen name="Sign In" component={Login} />
            <Stack.Screen name="Sign Up" component={SignUp} />
        </Stack.Navigator>
    )
}

const Stack = createStackNavigator();

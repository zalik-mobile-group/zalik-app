import React, {useState, useEffect} from 'react';
import {Button, Text, View, TouchableOpacity, Switch, Image, Picker} from "react-native";
import {useTheme} from "@react-navigation/native";
import {StyleSheetFactory} from "../../../styles/styles";
import firebase from "../../../config/firebase/firebase";

const db = firebase.firestore();

export function Settings({navigation, route}) {
    let onSnapshotUnsubscribe;
    const {colors} = useTheme();
    const styles = StyleSheetFactory.getSheet(colors);
    const [isThemeSwitched, setIsThemeSwitched] = useState(false);
    const [userId, setUserId] = useState(firebase.auth().currentUser.uid);
    const {difficulty, onclickFunction} = route.params
    const [diff, setDiff] = useState(route.params.difficulty)

    const onToggleSwitch = () => {
        db.collection("app-db")
            .doc(userId).set({
            isThemeSwitched: !isThemeSwitched
        });
    };

    useEffect(() => {
        db.collection("app-db")
            .doc(userId)
            .get()
            .then((snapshot) => {
                const data = snapshot.data();
                setIsThemeSwitched(data.isThemeSwitched);
            });

        onSnapshotUnsubscribe =
            db.collection("app-db")
                .doc(userId)
                .onSnapshot((snapshot) => {
                    const data = snapshot.data();
                    setIsThemeSwitched(data.isThemeSwitched);
                });

        return () => {
            if (onSnapshotUnsubscribe !== undefined) {
                onSnapshotUnsubscribe();
            }
        }
    }, []);

    return (
        <View>
            <View style={{
                display: 'flex',
                flexDirection: 'row',
                alignItems: 'center',
                marginBottom: 30,
                height: 100
            }}>
                <View style={{
                    display: 'flex',
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginBottom: 30,
                    width: '100%'
                }}>
                    {isThemeSwitched ?
                        <Image
                            style={{
                                height: 30,
                                width: 30,
                                marginLeft: 25
                            }}
                            source={require("../../../assets/light.png")}
                        />
                        :
                        <Image
                            style={{
                                height: 30,
                                width: 30,
                                marginLeft: 25
                            }}
                            source={require("../../../assets/dark.png")}
                        />
                    }
                    <Text style={{
                        color: colors.text,
                        marginLeft: 10
                    }}>{isThemeSwitched ? 'Dark theme' : 'Light theme'}</Text>
                    <View style={{
                        position: 'absolute',
                        right: 0,
                        marginRight: 25
                    }}>
                        <Switch
                            trackColor={{false: "#767577", true: "#81b0ff"}}
                            thumbColor={isThemeSwitched ? "#f5dd4b" : "#f4f3f4"}
                            ios_backgroundColor="#3e3e3e"
                            onValueChange={onToggleSwitch}
                            value={isThemeSwitched}
                        />
                    </View>
                </View>
            </View>
            <Picker
                selectedValue={difficulty}
                style={{height: 50, width: 150}}
                onValueChange={(itemValue, itemIndex) => {
                    // setDiff(itemValue)
                    onclickFunction(itemValue)
                }}
            >
                <Picker.Item label="Easy" value="easy"/>
                <Picker.Item label="Medium" value="medium"/>
                <Picker.Item label="Hard" value="hard"/>
            </Picker>
        </View>
    )
}

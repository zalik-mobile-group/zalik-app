import React, {useState, useEffect} from 'react';
import {Text, View} from "react-native";
import {useTheme} from "@react-navigation/native";
import {StyleSheetFactory} from "../../../styles/styles";
import firebase from "../../../config/firebase/firebase";
import {SafeAreaView, ScrollView} from "react-native-web";

const db = firebase.firestore();

export function Statistics({navigation, route}) {
    // let stats;
    // stats = [];
    const {tab} = route.params
    const [stats, setStats] = useState([]);
    const {colors} = useTheme();
    const styles = StyleSheetFactory.getSheet(colors);
    const Item = ({userName, score, time}) => (
        <View style={styles.statisticsItem}>
            <Text style={styles.statisticsItemTitle}>{userName}</Text>
            <Text style={styles.statisticsItemTitle}>{score}</Text>
            <Text style={styles.statisticsItemTitle}>{time}</Text>
        </View>
    );

    const renderItem = ({item}) => {
        return (
            <Item
                item={item}
                onPress={() => {
                }}
                style={{backgroundColor: colors.primary}}
            />
        );
    };

    useEffect(() => {
        let collection = "";
        switch (tab) {
            case "easy": {
                collection = "users-stats"
                break;
            }
            case "medium": {
                collection = "users-stats-med"
                break;
            }
            case "hard": {
                collection = "users-stats-hard"
                break;
            }
        }
        db.collection(collection)
            .get()
            .then(function (querySnapshot) {
                querySnapshot.forEach(function (doc) {
                    const statElem = {
                        id: doc.id,
                        ...doc.data()
                    };
                    setStats(stats => [...stats, statElem])
                });
                // const temp = stats;
                // console.log(temp)
                //

                //  setStats(temp)
            })
    }, []);
// a mykova xutpuu pes
    return (
        <View>
            <SafeAreaView styles={styles.statisticsContainer}>
                <View style={{width: '100%', alignItems: 'center', justifyContent: 'center'}}>
                    <Text style={{
                        color: colors.text,
                        fontSize: 35,
                        fontWeight: '600',
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}>Top Players</Text>
                </View>
                <ScrollView>
                    <View
                        style={{
                            display: 'flex',
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'center',
                            backgroundColor: "#c1c1c1",
                            margin: 10,
                            borderRadius: 5,
                            height: 50
                        }}>
                        <Text style={{
                            position: 'absolute',
                            left: 25,
                            fontSize: 25,
                            alignItems: 'center',
                            fontWeight: 500
                        }}>User name</Text>
                        <Text style={{
                            position: 'absolute',
                            right: 25,
                            fontSize: 25,
                            alignItems: 'center',
                            fontWeight: 500
                        }}>
                            {tab === 'medium' || tab === 'hard' ? <Text>Score</Text> :
                                <View style={{
                                    display: 'flex',
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                    justifyContent: 'center'
                                }}>
                                    <Text>Time</Text>
                                </View>
                            }
                        </Text>
                    </View>
                    {
                        stats.sort((a, b) => {
                            if (tab === "easy") {
                                if (a.time > b.time) {
                                    return 1;
                                }
                                if (a.time < b.time) {
                                    return -1;
                                }
                            } else if (tab === "medium" || tab === "hard") {
                                if (a.score < b.score) {
                                    return 1;
                                }
                                if (a.score > b.score) {
                                    return -1;
                                }
                            }
                        }).map(item =>
                            <View
                                key={item.id}
                                style={{
                                    display: 'flex',
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    backgroundColor: "#c1c1c1",
                                    margin: 10,
                                    borderRadius: 5,
                                    height: 50
                                }}>
                                <Text style={{
                                    position: 'absolute',
                                    left: 25,
                                    fontSize: 25,
                                    alignItems: 'center',
                                    fontWeight: 500
                                }}>{item.userName}</Text>
                                <Text style={{
                                    position: 'absolute',
                                    right: 25,
                                    fontSize: 25,
                                    alignItems: 'center',
                                    fontWeight: 500
                                }}>
                                    {tab === 'medium' || tab === 'hard' ? item.score :
                                        <View style={{
                                            display: 'flex',
                                            flexDirection: 'row',
                                            alignItems: 'center',
                                            justifyContent: 'center'
                                        }}>
                                            {Math.floor(item.time / 60) < 10 ? <Text style={{
                                                marginLeft: 5,
                                                alignItems: 'center',
                                                justifyContent: 'center'
                                            }}>0{Math.floor(item.time / 60)}</Text> : Math.floor(item.time / 60)}
                                            :
                                            {(item.time - (Math.floor(item.time / 60) * 60)) < 10 ? <Text style={{
                                                alignItems: 'center',
                                                justifyContent: 'center'
                                            }}>0{(item.time - (Math.floor(item.time / 60) * 60))}</Text> : (item.time - (Math.floor(item.time / 60) * 60))}

                                        </View>
                                    }
                                </Text>
                            </View>
                        )
                    }
                </ScrollView>
            </SafeAreaView>
        </View>
    )
}

import React from "react";
import {View, Button, TouchableOpacity} from "react-native";
import {useTheme} from "@react-navigation/native";
import {Entypo, FontAwesome, Ionicons} from "@expo/vector-icons";
import Card from "./components/Card";
import Header from "./components/Header";
import Score from "./components/Score";
import helpers from "./util/helpers";
import {StyleSheetFactory} from "../../../../styles/styles";
import {Text} from "react-native-web";

function wrap(Component) {
    return function WrappedComponent(props) {
        const {colors} = useTheme()
        return <Component {...props} colors={colors}/>;
    }
}

class Game extends React.Component {
    constructor(props) {
        super(props);
        this.renderCards = this.renderCards.bind(this);
        this.resetCards = this.resetCards.bind(this);
        this.customShuffle = this.customShuffle.bind(this);

        let sources = {
            'fontawesome': FontAwesome,
            'entypo': Entypo,
            'ionicons': Ionicons
        };

        let cards = [
            {
                src: 'entypo',
                name: 'camera',
                color: 'red'
            },
            {
                src: 'entypo',
                name: 'key',
                color: '#127d52'
            },
            {
                src: 'entypo',
                name: 'game-controller',
                color: 'purple'
            },
            {
                src: 'entypo',
                name: 'aircraft',
                color: '#37b24d'
            },
            {
                src: 'entypo',
                name: 'trophy',
                color: 'brown'
            },
            {
                src: 'entypo',
                name: 'tree',
                color: '#FF0000'
            },
            {
                src: 'entypo',
                name: 'bug',
                color: '#5f5f5f'
            },
            {
                src: 'entypo',
                name: 'database',
                color: this.props.colors.github
            },
            {
                src: 'entypo',
                name: 'battery',
                color: '#1686D9'
            },
            {
                src: 'entypo',
                name: 'landline',
                color: '#1c7cd6'
            },
            {
                src: 'entypo',
                name: 'map',
                color: '#d61c1c'
            },
            {
                src: 'entypo',
                name: 'trash',
                color: '#3C5B9B'
            }
        ];

        let clone = JSON.parse(JSON.stringify(cards));

        this.cards = cards.concat(clone);
        this.cards.map((obj) => {
            let id = Math.random().toString(36).substring(7);
            obj.id = id;
            obj.src = sources[obj.src];
            obj.is_open = false;
        });

        this.cards = this.cards.shuffle();
        this.state = {
            current_selection: [],
            selected_pairs: [],
            score: 0,
            cards: this.cards,
            seconds: 0,
            minutes: 0,
            time: 0,
            intervalId: null,
            hard: 300,
            medium: 120
        }

        this.countUp = this.countUp.bind(this);
        this.countDown = this.countDown.bind(this);

        // this.minutes = this.minutes.bind(this);
        // this.seconds = this.seconds.bind(this);

    }

    countUp() {
        let sec = this.state.seconds
        sec += 1
        if (sec === 60) {
            sec = 0
            this.setState(({seconds: sec, minutes: this.state.minutes += 1}));
        } else {
            this.setState(({seconds: sec}));
        }
    }

    countDown() {
        console.log("timer", this.state.minutes, this.state.seconds)
        let min = this.state.minutes
        let sec = this.state.seconds
        console.log("timer", min, sec)
        if (this.state.minutes === 0 && this.state.seconds === 0) {
            clearInterval(this.state.intervalId);
            this.props.navigation.reset({
                index: 0,
                routes: [{
                    name: "GameOver", params: {
                        minutes: this.state.minutes,
                        seconds: this.state.seconds,
                        score: this.state.score,
                        level: true,
                        difficulty: this.props.route.params.difficulty
                    }
                }],
            })
            // this.props.navigation.navigate("GameOver", {
            //         minutes: this.state.minutes,
            //         seconds: this.state.seconds,
            //         score: this.state.score,
            //         level: true,
            //         difficulty: this.props.route.params.difficulty
            //     }
            // );
        } else if (this.state.seconds === 0) {
            console.log(this.state.minutes, this.state.seconds)
            this.setState({minutes: this.state.minutes - 1, seconds: 59})
        } else {
            console.log(this.state.minutes, this.state.seconds)
            this.setState({seconds: this.state.seconds - 1})
        }
    }

    componentDidMount() {
        if (this.props.route.params.difficulty === 'easy') {
            let intervalId = setInterval(this.countUp, 1000);
            this.setState({intervalId: intervalId});
        } else if (this.props.route.params.difficulty === 'medium') {
            this.setState({minutes: 0})
            this.setState({seconds: 30})
            let intervalId = setInterval(this.countDown, 1000);
            this.setState({intervalId: intervalId});
        } else if (this.props.route.params.difficulty === 'hard') {
            this.setState({minutes: 1});
            this.setState({seconds: 0});
            let intervalId = setInterval(this.countDown, 1000);
            this.setState({intervalId: intervalId});
        }
    }

    render() {
        const colors = this.props.colors;
        const styles = StyleSheetFactory.getSheet(colors)
        console.log(this.state.time)

        let CardSource = Entypo;
        let icon_name = 'list';
        let icon_color = colors.gameElem;
        let clock_icon_name = 'clock';
        let clock_icon_color = colors.gameElem;
        let score_icon_name = 'hair-cross';
        let score_icon_color = colors.gameElem;

        const {difficulty} = this.props.route.params

        console.log(difficulty)

        return (
            <View style={styles.gameContainer}>
                <View style={styles.timeAndScoreContainer}>
                    <TouchableOpacity
                        style={{padding: 10, position: 'absolute', left: 10}}
                        onPress={() => {
                            clearInterval(this.state.intervalId);
                            this.props.navigation.reset({
                            index: 0,
                            routes: [{name: "Menu"}],
                        })}}
                    >
                        <CardSource
                            name={icon_name}
                            size={50}
                            color={icon_color}
                        />
                    </TouchableOpacity>
                    <Text style={{
                        marginLeft: 20,
                        fontSize: 30,
                        fontWeight: '500',
                        padding: 10,
                        color: colors.text,
                        alignItems: 'center',
                        justifyContent: 'center',
                        flexDirection: 'row',
                        marginRight: 15
                    }}>
                        <CardSource
                            name={clock_icon_name}
                            size={30}
                            color={clock_icon_color}
                        />
                        {this.state.minutes < 10 ? <Text style={{
                            marginLeft: 5,
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}>0{this.state.minutes}</Text> : this.state.minutes}
                        :
                        {this.state.seconds < 10 ? <Text style={{
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}>0{this.state.seconds}</Text> : this.state.seconds}
                    </Text>
                    <Text style={{
                        fontSize: 30,
                        fontWeight: '500',
                        padding: 10,
                        color: colors.text,
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}>
                        <CardSource
                            name={score_icon_name}
                            size={30}
                            color={score_icon_color}
                        /><Text style={{marginLeft: 5}}>{this.state.score}</Text></Text>
                </View>
                <View style={styles.gameBody}>
                    {
                        this.renderRows.call(this)
                    }
                </View>
            </View>
        );
    }


    resetCards() {
        let cards = this.cards.map((obj) => {
            obj.is_open = false;
            return obj;
        });

        cards = cards.shuffle();

        this.setState({
            current_selection: [],
            selected_pairs: [],
            cards: cards,
            score: 0
        });
    }


    renderRows() {
        const colors = this.props.colors
        const styles = StyleSheetFactory.getSheet(colors)
        let contents = this.getRowContents(this.state.cards);
        return contents.map((cards, index) => {
            return (
                <View key={index} style={styles.gameRow}>
                    {this.renderCards(cards)}
                </View>
            );
        });

    }


    renderCards(cards) {
        return cards.map((card, index) => {
            return (
                <Card
                    colors={this.props.colors}
                    key={index}
                    src={card.src}
                    name={card.name}
                    color={card.color}
                    is_open={card.is_open}
                    clickCard={this.clickCard.bind(this, card.id)}
                />
            );
        });
    }

    customShuffle(array) {
        var i = array.length, j, temp;
        if (i == 0) return array;
        while (--i) {
            j = Math.floor(Math.random() * (i + 1));
            temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }
        return array;
    }

    clickCard(id) {
        let selected_pairs = this.state.selected_pairs;
        let current_selection = this.state.current_selection;
        let score = this.state.score;

        let index = this.state.cards.findIndex((card) => {
            return card.id == id;
        });

        let cards = this.state.cards;

        if (cards[index].is_open == false && selected_pairs.indexOf(cards[index].name) === -1) {

            cards[index].is_open = true;

            current_selection.push({
                index: index,
                name: cards[index].name
            });

            if (current_selection.length == 2) {
                if (current_selection[0].name == current_selection[1].name) {
                    score += 1;
                    if (this.props.route.params.difficulty === 'easy') {
                        if (score === 3) {
                            this.props.navigation.reset({
                                index: 0,
                                routes: [{
                                    name: "GameOver", params: {
                                        minutes: this.state.minutes,
                                        seconds: this.state.seconds,
                                        score: score,
                                        level: false,
                                        difficulty: this.props.route.params.difficulty
                                    }
                                }],
                            })
                            // this.props.navigation.navigate("GameOver", {
                            //         minutes: this.state.minutes,
                            //         seconds: this.state.seconds,
                            //         score: score
                            //     }
                            // );
                            clearInterval(this.state.intervalId);
                        }
                    } else {
                        if (score !== 0 && (score % 2) === 0) {
                            let cards = this.cards.shuffle().map((obj) => {
                                obj.is_open = false;
                                return obj;
                            });
                            console.log(cards)

                            this.setState({
                                current_selection: [],
                                selected_pairs: [],
                                cards: cards,
                            });

                            this.renderCards(cards)
                            this.renderRows(cards)
                        }
                    }
                    selected_pairs.push(cards[index].name);
                } else {

                    cards[current_selection[0].index].is_open = false;

                    setTimeout(() => {
                        cards[index].is_open = false;
                        this.setState({
                            cards: cards
                        });
                    }, 500);
                }

                current_selection = [];
            }

            this.setState({
                score: score,
                cards: cards,
                current_selection: current_selection
            });

        }

    }


    getRowContents(cards) {
        let contents_r = [];
        let contents = [];
        let count = 0;
        cards.forEach((item) => {
            count += 1;
            contents.push(item);
            if (count == 4) {
                contents_r.push(contents)
                count = 0;
                contents = [];
            }
        });

        return contents_r;
    }
}

export default wrap(Game)

import React from 'react';
import {StyleSheet, View, TouchableHighlight} from 'react-native';
import {Entypo, FontAwesome} from '@expo/vector-icons';
import {StyleSheetFactory} from "../../../../../styles/styles"; // 6.2.2

export default class Card extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        const colors = this.props.colors
        const styles = StyleSheetFactory.getSheet(colors)
        let CardSource = Entypo;
        let icon_name = 'mask';
        let icon_color = colors.gameElem;

        if (this.props.is_open) {
            CardSource = this.props.src;
            icon_name = this.props.name;
            icon_color = this.props.color;
        }

        return (
            <View style={styles.card}>
                <TouchableHighlight onPress={this.props.clickCard} activeOpacity={0.75} underlayColor={"#f1f1f1"}>
                    <CardSource
                        name={icon_name}
                        size={50}
                        color={icon_color}
                    />
                </TouchableHighlight>
            </View>
        );
    }
}


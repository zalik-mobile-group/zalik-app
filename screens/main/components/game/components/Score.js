import React from 'react';
import {Text, View} from 'react-native';
import {StyleSheetFactory} from "../../../../../styles/styles";

export default class Score extends React.Component {

    render() {
        const colors = this.props.colors;
        const styles = StyleSheetFactory.getSheet(colors)
        return (
            <View style={styles.score_container}>
                <Text style={styles.score}>Score: {this.props.score}</Text>
            </View>
        );
    }
}


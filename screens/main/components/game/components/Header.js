import React from 'react';
import {Text, View } from 'react-native';
import {StyleSheetFactory} from "../../../../../styles/styles";

export default class Header extends React.Component {

    render() {
        const colors = this.props.colors
        const styles = StyleSheetFactory.getSheet(colors)
        return (
            <View style={styles.gameHeader}>
                <Text style={styles.gameHeaderText}>Larva Game</Text>
            </View>
        );
    }
}

import React, {useEffect} from "react";
import {View, Button, Text, TouchableOpacity} from "react-native";
import {useTheme} from "@react-navigation/native";
import {StyleSheetFactory} from "../../../../styles/styles";
import {Entypo} from "@expo/vector-icons";
import firebase from "../../../../config/firebase/firebase";

const db = firebase.firestore();

const GameOver = ({navigation, route}) => {
    const {colors} = useTheme();
    const styles = StyleSheetFactory.getSheet(colors);
    const {minutes, seconds, score, difficulty, level} = route.params

    let CardSource = Entypo;
    let icon_name = 'trophy';
    let icon_color = '#ffd43b';

    useEffect(() => {
        firebase.auth().onAuthStateChanged((user) => {
            let collectionName = "users-stats";
            switch (difficulty) {
                case "easy": {
                    collectionName = "users-stats"
                    break;
                }
                case "medium": {
                    collectionName = "users-stats-med"
                    break;
                }
                case "hard": {
                    collectionName = "users-stats-hard"
                    break;
                }
            }
            db.collection(collectionName)
                .doc(user.uid)
                .get()
                .then((snapshot) => {
                    const data = snapshot.data()
                    if (data === undefined || data.time > minutes * 60 + seconds) {
                        db.collection(collectionName).doc(user.uid).set({
                            score: score,
                            time: minutes * 60 + seconds,
                            userName: user.displayName
                        });
                    }
                });
        })
    }, [])

    return (
        <View style={styles.container}>
            <View style={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'center',
                marginBottom: 20
            }}>
                <Text style={{
                    fontSize: 35,
                    padding: 10,
                    color: colors.text,
                }}>Congratulations</Text>
                <CardSource
                    name={icon_name}
                    size={100}
                    color={icon_color}
                />
                {level ?
                    <Text style={styles.timeScore}>
                        Time is up
                    </Text> :
                    <Text style={styles.timeScore}>
                        Time:{" "}
                        {minutes < 10 ? <Text>0{minutes}</Text> : minutes}
                        :
                        {seconds < 10 ? <Text>0{seconds}</Text> : seconds}
                    </Text>
                }
                <Text style={{
                    fontSize: 25,
                    padding: 10,
                    color: colors.text,
                }}>Score: {score}</Text>
                {/*<Score colors={this.props.colors} score={this.state.score}/>*/}
            </View>
            <TouchableOpacity
                style={styles.menuButton}
                onPress={() => navigation.reset({
                    index: 0,
                    routes: [{name: "Menu"}],
                })}
            >
                <Text style={styles.buttonText}>Go to Menu</Text>
            </TouchableOpacity>
            <TouchableOpacity
                style={{
                    width: 200,
                    height: 60,
                    margin: 5,
                    borderRadius: 5,
                    backgroundColor: '#53d769',
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                }}
                onPress={() => navigation.reset({
                    index: 0,
                    routes: [{name: "Play", params: {difficulty: difficulty}}],
                })}
            >
                <Text style={styles.buttonText}>Play again</Text>
            </TouchableOpacity>
        </View>
    )
}

export default GameOver;

import React, {useState, useEffect} from 'react';
import {Button, Text, View, TouchableOpacity} from "react-native";
import {useTheme} from "@react-navigation/native";
import {StyleSheetFactory} from "../../../styles/styles";
import firebase from "../../../config/firebase/firebase";

export function Menu({navigation, route}) {
    const {colors} = useTheme();
    const styles = StyleSheetFactory.getSheet(colors);
    const [difficulty, setDifficulty] = useState('easy');

    const onPressLogOut = () => {
        firebase.auth().signOut().then(() => {
            navigation.reset({
                index: 0,
                routes: [{name: "Auth"}],
            });
        })
            .catch((error) => console.log(error))

    };

    return (
        <View style={styles.container}>
            <TouchableOpacity
                style={{
                    width: 200,
                    height: 60,
                    margin: 5,
                    borderRadius: 5,
                    backgroundColor: '#53d769',
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                }}
                onPress={() => navigation.navigate('Play', {
                        difficulty: difficulty
                    }
                )}
            >
                <Text style={styles.buttonText}>Play</Text>
            </TouchableOpacity>
            <TouchableOpacity
                style={styles.menuButton}
                onPress={() =>
                    navigation.navigate("Settings", {
                            difficulty: difficulty,
                            onclickFunction: setDifficulty,
                        }
                    )
                }
            >
                <Text style={styles.buttonText}>Settings</Text>
            </TouchableOpacity>
            <TouchableOpacity
                style={styles.menuButton}
                onPress={() => navigation.navigate('Statistics')}
            >
                <Text style={styles.buttonText}>Statistics</Text>
            </TouchableOpacity>
            <TouchableOpacity
                style={styles.menuButton}
                onPress={onPressLogOut}
            >
                <Text style={styles.buttonText}>Logout</Text>
            </TouchableOpacity>
        </View>
    )
}

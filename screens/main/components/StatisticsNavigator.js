import React from 'react';
import {createBottomTabNavigator} from "@react-navigation/bottom-tabs";
import {Statistics} from "./Statistics";

export default function StatisticsNavigator() {
    return (
        <Tab.Navigator>
            <Tab.Screen name={"Easy"} component={Statistics} initialParams={{tab: "easy"}}/>
            <Tab.Screen name={"Medium"} component={Statistics} initialParams={{tab: "medium"}}/>
            <Tab.Screen name={"Hard"} component={Statistics} initialParams={{tab: "hard"}}/>
        </Tab.Navigator>
    )
}

const Tab = createBottomTabNavigator();

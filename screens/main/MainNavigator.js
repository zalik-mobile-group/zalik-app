import React from 'react';
import {createStackNavigator} from "@react-navigation/stack";
import {Menu} from "./components/Menu";
import {Settings} from "./components/Settings";
import {Statistics} from "./components/Statistics";
import Game from "./components/game/Game";
import GameOver from "./components/game/GameOver";
import {Text, TouchableOpacity} from "react-native";
import {useTheme} from "@react-navigation/native";
import {StyleSheetFactory} from "../../styles/styles";
import {Entypo} from "@expo/vector-icons";
import StatisticsNavigator from "./components/StatisticsNavigator";

export function MainNavigator({navigation}) {
    const {colors} = useTheme();
    const styles = StyleSheetFactory.getSheet(colors);

    return (
        <Stack.Navigator>
            <Stack.Screen name="Menu" component={Menu} options={{ headerShown: false }}/>
            <Stack.Screen name="GameOver" component={GameOver} options={{ headerShown: false }}/>
            <Stack.Screen name="Play" component={Game} options={{headerShown: false}}/>
            <Stack.Screen name="Settings" component={Settings}/>
            <Stack.Screen name="Statistics" component={StatisticsNavigator}/>
        </Stack.Navigator>
    )
}

const Stack = createStackNavigator();

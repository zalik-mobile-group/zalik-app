import React from 'react';
import {Text, TextInput, View} from "react-native";
import {useTheme} from "@react-navigation/native";
import {StyleSheetFactory} from "../styles/styles";

const InputField = (props) => {
    const {colors} = useTheme();
    const styles = StyleSheetFactory.getSheet(colors);

    return (
        <>
            <Text style={styles.InputFieldTitle}>{props.title}</Text>
            <View style={styles.InputFieldContainer}>
                <TextInput
                    style={styles.InputField}
                    onChangeText={props.change}
                    onBlur={props.blur}
                    placeholder={props.placeholder}
                    placeholderTextColor={colors.placeholderText}
                    placeholderStyle={{ fontWeight: "700" }}
                    value={props.value}
                    touched={props.touched}
                    errors={props.error}
                    secureTextEntry={props.title === 'Password'}
                />
            </View>
            {props.touched && props.error && <Text style={styles.ErrorMessage}>{props.error}</Text>}
        </>
    )
};

export default InputField;

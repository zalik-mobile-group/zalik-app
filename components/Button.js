import React from 'react';
import {Text, TouchableOpacity} from "react-native";

const Button = (props) => {
    return (
        <TouchableOpacity
            style={props.btnStyles}
            onPress={props.onClick}
        >
            <Text style={props.btnTextStyles}>
                {props.title}
            </Text>
        </TouchableOpacity>
    )
};

export default Button;

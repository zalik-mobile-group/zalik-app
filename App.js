import React, {useEffect, useState} from 'react';
import {createStackNavigator} from "@react-navigation/stack";
import {DarkTheme, DefaultTheme, NavigationContainer} from "@react-navigation/native";
import {AuthNavigator} from "./screens/auth/AuthNavigator";
import {MainNavigator} from "./screens/main/MainNavigator";
import firebase from "./config/firebase/firebase";

const db = firebase.firestore();

const MyLightTheme = {
    ...DefaultTheme,
    colors: {
        ...DefaultTheme.colors,
        primary: '#F2F2F2', // menu button
        background: 'rgb(234,233,233)',
        text: '#000000',
        placeholderText: '#7a8898',
        secondary: '#dae0e7',
        error: "#f52318",
        gameElem: "#00558f",
        gameBackground: '#ffffff',
        github: "#24292e",
        cardBackground: "#ffa500"
    },
};

const MyDarkTheme = {
    ...DarkTheme,
    colors: {
        ...DarkTheme.colors,
        primary: '#1B1C21', // menu button
        background: '#151419',
        text: '#ffffff',
        placeholderText: '#7a8898',
        secondary: '#262729',
        error: "#f52318",
        gameElem: "#cbcbcb",
        gameBackground: "#1B1C21",
        github: "#ffffff",
        cardBackground: "#010f37"
    },
};

export default function App() {
   let onSnapshotUnsubscribe;
    const [theme, setTheme] = useState(MyLightTheme)
    useEffect(() => {
        firebase.auth().onAuthStateChanged((user) => {
            if (user) {
                db.collection("app-db")
                    .doc(user.uid)
                    .get()
                    .then((snapshot) => {
                        const data = snapshot.data()
                        if (data === undefined) {
                            db.collection("app-db")
                                .doc(user.uid).set({
                                isThemeSwitched: false
                            });
                        } else {
                            if (snapshot.data().isThemeSwitched) {
                                setTheme(MyDarkTheme)
                            } else {
                                setTheme(MyLightTheme)
                            }
                        }
                    });

                onSnapshotUnsubscribe =
                    db.collection("app-db")
                        .doc(user.uid)
                        .onSnapshot((snapshot) => {
                            const data = snapshot.data()
                            if (data !== undefined) {
                                if (data.isThemeSwitched) {
                                    setTheme(MyDarkTheme)
                                } else {
                                    setTheme(MyLightTheme)
                                }
                            }
                        });
            }
        });
        return () => {
            if (onSnapshotUnsubscribe !== undefined) {
                onSnapshotUnsubscribe();
            }
        }
    }, []);

    return (
        <NavigationContainer theme={theme}>
            <Stack.Navigator headerMode={"none"}>
                <Stack.Screen name="Auth" component={AuthNavigator}/>
                <Stack.Screen name="Main" component={MainNavigator}/>
            </Stack.Navigator>
        </NavigationContainer>
    );
}

const Stack = createStackNavigator();
